function down() {
    changeMainPhoto();
    var li1 = $("<li>").append($("<a>").attr("href","/interests#old").text("Старые"));
    var li2 = $("<li>").append($("<a>").attr("href","/interests#new").text("Новые"));
    var ul = $("<ul>").append(li1).append(li2);
    var div = $("<div id='dropdown'>").append(ul);
    $("#interest").append(div);
}

function remove() {
    returnMainPhoto();
    $("#dropdown").remove();
}

function changeMainPhoto() {
    $("#avatar").attr("src", "/img/secondAva.jpg");
}

function returnMainPhoto() {
    $("#avatar").attr("src", "/img/ava.jpg");
}

$("#nav").children()
    .mouseenter(function () {
        changeMainPhoto();
    })
    .mouseleave(function () {
        returnMainPhoto();
});

$("#interest")
    .mouseenter(function () {
        down();
    })
    .mouseleave(function () {
        remove();
});

