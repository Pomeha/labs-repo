<?php

class Home extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->className = strtolower(__CLASS__) . '/';
    }

    public function index()
    {
        if(!empty($_SESSION['timeLogout'])) {
            if($_SESSION['timeLogout'] < time()) {
                session_destroy();
                header("Location: " . $_SERVER['HTTP_REFERER']);
                exit();
            } else {
                $_SESSION['timeLogout'] = time() + 10;
            }
        }
        $userInfo = $this->getUserInfo();
        $isAdmin = $this->isInRole("Admin");
        $this->saveVisitInformation("index");
        $this->view->generate($this->className . 'index', compact("userInfo", "isAdmin"));
    }

    public function login()
    {
        $errors = [];
        if (!empty($_POST)) {
            $errors = TestValidation::run([
                "login" => "required",
                "password" => "required"
            ]);
            if (empty($errors)) {
                $user = UserModel::getUser($_POST['login'], hash("sha256", $_POST['password']));
                if ($user == null) {
                    $loginError = "Неверный логин либо пароль, попробуйте еще раз!";
                } else {
                    $_SESSION["user"] = $user;
                    $_SESSION["timeLogout"] =  time() + 10;
                    header("Location: " . "/home");
                    exit;
                }
            }
        }
        $this->saveVisitInformation("login");
        $this->view->generate($this->className . 'login', compact("errors", "loginError"));
    }

    public function register()
    {
        $errors = [];
        if (!empty($_POST)) {
            $errors = TestValidation::run([
                "login" => "required,login",
                "password" => "required",
                "fio" => "required,fio",
                "email" => "email"
            ]);
            if (empty($errors)) {
                $user = new UserModel();
                $user->Login = $_POST['login'];
                $user->Password = hash("sha256", $_POST['password']);
                $user->Fio = $_POST['fio'];
                $user->Email = $_POST['email'];
                $user->Role_Id = 2; //Обычный пользователь
                $user->save();
                header("Location: " . "/home");
            }
        }
        $this->saveVisitInformation("register");
        $this->view->generate($this->className . 'register', compact("errors"));
    }

    public function logout()
    {
        session_destroy();
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }

    public static function showError($error)
    {
        echo $error;
    }

    public static function printInterests()
    {
        self::$names = array("Любимая музыка", "Фильмы", "Книги", "Игры");
        self::$anchors = array("Music", "Film", "Book", "Game");
        for ($i = 0; $i < 11; $i++) {
            self::$iphot[$i] = "/img/" . ($i + 1) . ".jpg";
        }
        for ($i = 0; $i < 4; $i++) {
            switch ($i) {
                case 0: {
                    printf("<h1 id='%s'>%s</h1>", self::$anchors[$i], self::$names[$i]);
                    for ($y = 1; $y < 4; $y++) {
                        printf("<img src='%s'>", self::$iphot[$y]);
                    }
                }
                    break;
                case 1: {
                    printf("<h1 id='%s'>%s</h1>", self::$anchors[$i], self::$names[$i]);
                    for ($y = 3; $y < 7; $y++) {
                        printf("<img src='%s'>", self::$iphot[$y]);
                    }
                }
                    break;
                case 2: {
                    printf("<h1 id='%s'>%s</h1>", self::$anchors[$i], self::$names[$i]);
                    for ($y = 6; $y < 8; $y++) {
                        printf("<img src='%s'>", self::$iphot[$y]);
                    }
                }
                    break;
                case 3: {
                    printf("<h1 id='%s'>%s</h1>", self::$anchors[$i], self::$names[$i]);
                    for ($y = 8; $y < 11; $y++) {
                        printf("<img src='%s'>", self::$iphot[$y]);
                    }
                }
                    break;

            }
        }
    }

    public function loginNotExist() {
        if(!empty($_POST)) {
            echo UserModel::loginNotExist($_POST['login']);
        }
    }
}