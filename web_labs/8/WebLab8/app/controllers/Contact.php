<?php

class Contact extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->className = strtolower(__CLASS__) . '/';
    }

    public function index()
    {
        $userInfo = $this->getUserInfo();
        $isAdmin = $this->isInRole("Admin");
        $errors = [];
        if (!empty($_POST)) {
            $errors = Validation::run([
                'name' => 'required',
                'sex' => 'required',
                'dob' => 'required',
                'email' => 'email',
                'phone' => 'required'
            ]);
        }
        $this->saveVisitInformation("index");
        $this->view->generate($this->className . 'index', compact('errors', "userInfo", "isAdmin"));
    }
}