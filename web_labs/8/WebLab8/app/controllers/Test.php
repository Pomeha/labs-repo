<?php

class Test extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->className = strtolower(__CLASS__) . '/';
    }

    public function index()
    {
        $userInfo = $this->getUserInfo();
        $isAdmin = $this->isInRole("Admin");
        $isAuthorize = $this->isInRole("Admin", "User");
        $errors = [];
        $responses = [];
        if (!empty($_POST)) {
            $errors = TestValidation::run([
                'name' => 'required,fio',
                'group' => 'required',
                'answer1' => 'required',
                'answer2' => 'required,float',
                'answer3' => 'required'
            ]);
            $responses = TestVerification::run([
                'answer1' => 'answer-one',
                'answer2' => 'answer-two',
                'answer3' => 'answer-three'
            ]);
            if (empty($errors)) {
                $model = new TestsModel();
                $model->Fio = $_POST['name'];
                $model->Group = $_POST['group'];
                $model->Answer1 = $_POST['answer1'];
                $model->Answer2 = $_POST['answer2'];
                $model->Answer3 = $_POST['answer3'];
                $model->IsAnswer1 = empty($responses['answer1']) ? 0 : 1;
                $model->IsAnswer2 = empty($responses['answer2']) ? 0 : 1;
                $model->IsAnswer3 = empty($responses['answer3']) ? 0 : 1;
                $model->save();
            }
        }
        $this->saveVisitInformation("index");
        $this->view->generate($this->className . 'index', compact('errors', 'responses', "userInfo", "isAuthorize", "isAdmin"));
    }

    public function table()
    {
        if($this->isInRole("Admin", "User")) {
            $this->saveVisitInformation("index");
            $models = TestsModel::getAll();
            $this->view->generate($this->className . 'table', compact('models'));
        } else {
            $this->view->generate('401');
        }
    }
}