<?php

class PhotoAlbum extends Controller
{
    private static $photos;
    private static $alts;

    public function __construct()
    {
        parent::__construct();
        $this->className = strtolower(__CLASS__) . '/';
    }

    public function index()
    {
        $userInfo = $this->getUserInfo();
        $isAdmin = $this->isInRole("Admin");
        $this->saveVisitInformation("index");
        $this->view->generate($this->className . 'index', compact("userInfo", "isAdmin"));
    }

    public static function printPhotos()
    {
        self::$photos = [];
        self::$alts = [];
        for ($i = 0; $i < 15; $i++) {
            self::$photos[$i] = "/img/gallery/" . ($i + 1) . ".jpg";
            self::$alts[$i] = "Фотография " . ($i + 1);
        }
        for ($i = 0; $i < count(self::$photos); $i++) {
            printf("<div class='item'>");
            printf("<img class='photo' src='%s' title='%s' alt='%s'>", self::$photos[$i], self::$alts[$i], self::$alts[$i]);
            printf("<span class='item_title'>%s</span>", self::$alts[$i]);
            printf("</div>");
        }
    }
}