<?php

class WatchHistory extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->className = strtolower(__CLASS__) . '/';
    }

    public function index()
    {
        $userInfo = $this->getUserInfo();
        $isAdmin = $this->isInRole("Admin");
        $this->saveVisitInformation("index");
        $this->view->generate($this->className . 'index', compact("userInfo", "isAdmin"));
    }
}