<?php

class Blog extends Controller
{
    private $countRecords = 10;

    public function __construct()
    {
        parent::__construct();
        $this->className = strtolower(__CLASS__) . '/';
    }

    public function index()
    {
        $userInfo = $this->getUserInfo();
        $isAuthorize = $this->isAuthorize();
        $isAdmin = $this->isInRole("Admin");
        if(empty($_GET["page"])) {
            $page = 0;
        } else {
            $page = $_GET["page"];
        }
        $records = BlogModel::paginate($page, $this->countRecords);
        $count = BlogModel::getCount();
        $countPages = round($count / $this->countRecords);
        $this->saveVisitInformation("index");
        $this->view->generate($this->className . 'index', compact("records", "countPages", "page", "userInfo", "isAdmin", "isAuthorize"));
    }

    public function addComment() {
        if(!empty($_GET)) {
            $data = new SimpleXMLElement($_GET['xml']);
            $comment = new CommentModel();
            $comment->Text = $data->comment;
            $comment->Blog_Id = $data->blogId;
            $comment->User_Id = $_SESSION['user']->Id;
            $comment->save();
            echo json_encode(CommentModel::getComments($data->blogId));
        }
    }

    public function editBlog() {
        if(!empty($_POST)) {
            $blog = BlogModel::get($_POST['blogId']);
            $blog->Topic = $_POST['title'];
            $blog->Message = $_POST['message'];
            $blog->save();

            $blog->Comments = CommentModel::getComments($blog->Id);

            $isAuthorize = $this->isAuthorize();
            $isAdmin = $this->isInRole("Admin");

            echo "<div class='blog-record' data-blog-id='$blog->Id'>";
            echo '<div class="blog-topic">';
            echo $blog->Topic;
            echo '</div>';
            echo '<div class="blog-content">';
            if(!empty($blog->PathToPhoto)) {
                echo '<div class="blog-photo">';
                echo "<img class= 'img-thumbnail' src='$blog->PathToPhoto' alt='$blog->Topic'>";
                echo '</div>';
            }
            echo '<div class="blog-message">';
            echo '<p>' . $blog->Message . '</p>';
            echo '</div>';
            echo '<div class="clearfix"></div>';
            echo '<div class="blog-footer">';
            echo '<div class="blog-author">';
            echo "$blog->Author $blog->CurrentDate";
            echo '</div>';
            echo '<div class="blog-actions">';
            if($isAuthorize) {
                echo "<a class='edit-blog-action' data-blog-id='$blog->Id'>Изменить</a>";
            }
            if($isAdmin) {
                echo " <a class='add-comment-action' data-blog-id='$blog->Id'>Добавить комментарий</a>";
            }
            echo '</div>';
            echo '</div>';
            echo '<div class="blog-comments">';
            if(!empty($blog->Comments)) {
                foreach ($blog->Comments as $comment) {
                    echo '<div class="blog-comment">';
                    echo '<div class="blog-comment-text">';
                    echo "<p style='word-wrap: break-word'>$comment->Text</p>";
                    echo '</div>';
                    echo '<div class="clearfix"></div>';
                    echo '<div class="blog-comment-footer">';
                    echo "$comment->Fio $comment->Date";
                    echo '</div>';
                    echo '</div>';
                }
            }
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }
    }
}