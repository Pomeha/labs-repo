<?php

class Interests extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->className = strtolower(__CLASS__) . '/';
    }

    public function index()
    {
        $userInfo = $this->getUserInfo();
        $isAdmin = $this->isInRole("Admin");
        $this->saveVisitInformation("index");
        $this->view->generate($this->className . 'index', compact("userInfo", "isAdmin"));
    }

    public static function printList($type)
    {
        $args = func_get_args();
        echo "<" . $type . "l>";
        for ($i = 1; $i < func_num_args(); $i++) {
            echo "<li>" . $args[$i] . "</li>";
        }
        echo "</" . $type . "l>";
    }
}