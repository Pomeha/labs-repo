<?php

class GuestBook extends Controller
{
    private $file;

    public function __construct()
    {
        parent::__construct();
        $this->className = strtolower(__CLASS__) . '/';
    }

    public function index()
    {
        $userInfo = $this->getUserInfo();
        $isAdmin = $this->isInRole("Admin");
        if (!empty($_POST)) {
            $this->file = fopen(APP_PATH . "public/files/message.inc", "a");
            $string = "\n" . date("d.m.y") . ";" .
                $_POST["name"] . ";" .
                $_POST["email"] . ";" .
                $_POST["review"];
            fwrite($this->file, $string);
            fclose($this->file);
        }
        $reviews = [];
        $this->file = fopen(APP_PATH . "public/files/message.inc", "r");
        while (!feof($this->file)) {
            $row = fgets($this->file);
            $message = explode(";", $row);
            $review = new ReviewModel($message[0], $message[1], $message[2], $message[3]);
            array_push($reviews, $review);
        }
        fclose($this->file);
        usort($reviews, function ($a, $b) {
            if ($a->Date == $b->Date) {
                return 0;
            }
            return ($a->Date < $b->Date) ? 1 : -1;
        });
        $this->saveVisitInformation("index");
        $this->view->generate($this->className . 'index', compact("reviews", "userInfo", "isAdmin"));
    }
}