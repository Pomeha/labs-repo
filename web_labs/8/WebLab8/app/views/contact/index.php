<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Контакт</title>
</head>

<body>
<nav>
    <div class='container'>
        <div class="user-banner">
            <div id="time"></div>
            <div class="user-info">
                <?php
                echo $args["userInfo"];
                ?>
            </div>
        </div>
        <div class="header">
            <ul class="nav nav-pills pull-right" id="nav">
                <li><a href="/">Главная страница</a></li>
                <li><a href="/about">Обо мне</a></li>
                <li id="interest"><a href="/interests">Мои интересы</a></li>
                <li><a href="/study">Учеба</a></li>
                <li><a href="/photoAlbum">Фотоальбом</a></li>
                <li class="active"><a href="/contact">Контакт</a></li>
                <li><a href="/test">Тест</a></li>
                <li><a href="/watchHistory">История просмотра</a></li>
                <li><a href="/guestBook">Гостевая книга</a></li>
                <li><a href="/blog">Блог</a></li>
                <?php
                if($args["isAdmin"]) {
                    echo '<li><a href="/admin">Админ</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>

<div class="blokkok">
    <p class="lead">Форма для обратной связи</p>
    <div class='center'>
        <form id="form" class="form-horizontal" role="form" method="post">
            <div class="form-group">
                <label for="form_fio" class="col-md-2 control-label">Ф.И.О.:</label>
                <div class="col-md-10">
                    <input type="text" name="name" class="form-control" id="form_fio"
                           placeholder="Фамилия Имя Отчество">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Пол:</label>
                <div class="col-md-8">
                    <label class="radio-inline">
                        <input type="radio" name="sex" id="form_man" value="man">
                        Мужской
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="sex" id="form_woman" value="woman">
                        Женский
                    </label>
                </div>
            </div>
            <div class="form-group" id="date">
                <label class="col-md-2 control-label">Дата рождения:</label>
                <div class="col-md-10">
                    <input type="text" name="dob" class="form-control" id="form_date" placeholder="Дата рождения">
                </div>
            </div>
            <div class="form-group">
                <label for="form_email" class="col-md-2 control-label">Email:</label>
                <div class="col-md-10">
                    <input type="email" name="email" class="form-control" id="form_email" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label for="form_phone" class="col-md-2 control-label">Телефон:</label>
                <div class="col-md-10">
                    <input type="text" name="phone" class="form-control" id="form_phone" placeholder="Телефон">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-6">
                    <input id="submit" type="submit" disabled value="Отправить" class="form-control btn btn-block btn-primary">
                </div>
                <div class="col-md-4">
                    <input id="reset" type="button" value="Очистить" class="form-control btn btn-block btn-default">
                </div>
            </div>
    </div>
    </form>
    <?php
    Validation::showErrors($args['errors']);
    ?>
</div>
<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/time.js"></script>
<script src="/js/navigation.js"></script>
<script src="/js/calendar.js"></script>
<script src="/js/functions.js"></script>
<script src="/js/modalWindow.js"></script>
<script src="/js/validationMessage.js"></script>
<script src="/js/contact.js"></script>
<script src="/js/history.js"></script>
</body>
</html>