<!DOCTYPE html>
<html lang='ru'>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Персональный сайт Ильи Мжачева. Обо мне</title>
</head>

<body>
<nav>
    <div class='container'>
        <div class="user-banner">
            <div id="time"></div>
            <div class="user-info">
                <?php
                echo $args["userInfo"];
                ?>
            </div>
        </div>
        <div class='header'>
            <ul class="nav nav-pills pull-right" id="nav">
                <li><a href="/">Главная страница</a></li>
                <li class="active"><a href="/about">Обо мне</a></li>
                <li id="interest"><a href="/interests">Мои интересы</a></li>
                <li><a href="/study">Учеба</a></li>
                <li><a href="/photoAlbum">Фотоальбом</a></li>
                <li><a href="/contact">Контакт</a></li>
                <li><a href="/test">Тест</a></li>
                <li><a href="/watchHistory">История просмотра</a></li>
                <li><a href="/guestBook">Гостевая книга</a></li>
                <li><a href="/blog">Блог</a></li>
                <?php
                if($args["isAdmin"]) {
                    echo '<li><a href="/admin">Админ</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<div class="blokkok">
    <div class='container'>
        <div class='content'>
            <p class='lead'>Автобиография</p>
            Родился в городе Харьков в 1997 году, учился в школе с английский уклоном, интерес к программированию появился в 8-ом классе, когда впервые изучил Borland Pascal, далее изучал C++, C#, java и ОС Android, на данный момент, занимаюсь изучением web, и это мой первый опыт
        </div>
    </div>
</div>
<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/time.js"></script>
<script src="/js/navigation.js"></script>
<script src="/js/history.js"></script>
</body>
</html>