<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Мои интересы</title>
</head>
<body>
<nav>
    <div class='container'>
        <div class="user-banner">
            <div id="time"></div>
            <div class="user-info">
                <?php
                echo $args["userInfo"];
                ?>
            </div>
        </div>
        <div class="header">
            <ul class="nav nav-pills pull-right" id="nav">
                <li><a href="/">Главная страница</a></li>
                <li><a href="/about">Обо мне</a></li>
                <li class="active" id="interest"><a href="/interests">Мои интересы</a></li>
                <li><a href="/study">Учеба</a></li>
                <li><a href="/photoAlbum">Фотоальбом</a></li>
                <li><a href="/contact">Контакт</a></li>
                <li><a href="/test">Тест</a></li>
                <li><a href="/watchHistory">История просмотра</a></li>
                <li><a href="/guestBook">Гостевая книга</a></li>
                <li><a href="/blog">Блог</a></li>
                <?php
                if($args["isAdmin"]) {
                    echo '<li><a href="/admin">Админ</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>

<<div class="blokkok">
    <ul class="lostablos">
        <li><a href="#Music">Музыка</a></li>
        <li><a href="#Film">Фильмы</a></li>
        <li><a href="#Book">Книги</a></li>
        <li><a href="#Game">Игры</a></li>
    </ul>
    <div class="lostablos">
        <?php
        Home::printInterests();
        ?>
    </div>
</div>
<footer class='footer'>
    <div class='container'>
        <p class="text-muted pull-right">Мжачев И.А. &copy; 2017</p>
    </div>
</footer>
<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/time.js"></script>
<script src="/js/navigation.js"></script>
<script src="/js/history.js"></script>
</body>
</html>