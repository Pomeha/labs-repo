<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Сайт Акименко Владислава. История просмотра</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
</head>
<body>
<nav>
    <div class='container'>
        <div class="user-banner">
            <div id="time"></div>
            <div class="user-info">
                <?php
                echo $args["userInfo"];
                ?>
            </div>
        </div>
        <div class="header">
            <ul class="nav nav-pills pull-right" id="nav">
                <li><a href="/">Главная страница</a></li>
                <li><a href="/about">Обо мне</a></li>
                <li id="interest"><a href="/interests">Мои интересы</a></li>
                <li><a href="/study">Учеба</a></li>
                <li><a href="/photoAlbum">Фотоальбом</a></li>
                <li><a href="/contact">Контакт</a></li>
                <li><a href="/test">Тест</a></li>
                <li class="active"><a href="/watchHistory">История просмотра</a></li>
                <li><a href="/guestBook">Гостевая книга</a></li>
                <li><a href="/blog">Блог</a></li>
                <?php
                if($args["isAdmin"]) {
                    echo '<li><a href="/admin">Админ</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>

<div class="blokkok">
    <table id="currentHistory" class="table table-bordered table-responsive table-hover">
        <thead>
        <tr>
            <th colspan="2">История текущего сеанса (Local Storage)</th>
        </tr>
        <tr>
            <th>Страница</th>
            <th>Количество посещений</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
    <table id="allTimeHistory" class="table table-bordered table-responsive table-hover">
        <thead>
        <tr>
            <th colspan="2">История посещений за все время (Cookie)</th>
        </tr>
        <tr>
            <th>Страница</th>
            <th>Количество посещений</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>

<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/time.js"></script>
<script src="/js/navigation.js"></script>
<script src="/js/history.js"></script>
</body>
</html>