<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Фотоальбом</title>
</head>

<body class="ff">
<nav>
    <div class='container'>
        <div class="user-banner">
            <div id="time"></div>
            <div class="user-info">
                <?php
                echo $args["userInfo"];
                ?>
            </div>
        </div>
        <div class="header">
            <ul class="nav nav-pills pull-right" id="nav">
                <li><a href="/">Главная страница</a></li>
                <li><a href="/about">Обо мне</a></li>
                <li id="interest"><a href="/interests">Мои интересы</a></li>
                <li><a href="/study">Учеба</a></li>
                <li class="active"><a href="/photoAlbum">Фотоальбом</a></li>
                <li><a href="/contact">Контакт</a></li>
                <li><a href="/test">Тест</a></li>
                <li><a href="/watchHistory">История просмотра</a></li>
                <li><a href="/guestBook">Гостевая книга</a></li>
                <li><a href="/blog">Блог</a></li>
                <?php
                if($args["isAdmin"]) {
                    echo '<li><a href="/admin">Админ</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<div class="content container">
    <p class="lead">Фотоальбом</p>
    <div id="gallery">
        <?php
        PhotoAlbum::printPhotos();
        ?>
    </div>
</div>

<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/jquery.hotkeys.js"></script>
<script src="/js/time.js"></script>
<script src="/js/navigation.js"></script>
<script src="/js/photoAlbum.js"></script>
<script src="/js/history.js"></script>
</body>
</html>