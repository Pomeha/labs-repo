<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Тест</title>
</head>

<body>
<nav>
    <div class='container'>
        <div class="user-banner">
            <div id="time"></div>
            <div class="user-info">
                <?php
                echo $args["userInfo"];
                ?>
            </div>
        </div>
        <div class="header">
            <ul class="nav nav-pills pull-right" id="nav">
                <li><a href="/">Главная страница</a></li>
                <li><a href="/about">Обо мне</a></li>
                <li id="interest"><a href="/interests">Мои интересы</a></li>
                <li><a href="/study">Учеба</a></li>
                <li><a href="/photoAlbum">Фотоальбом</a></li>
                <li><a href="/contact">Контакт</a></li>
                <li class="active"><a href="/test">Тест</a></li>
                <li><a href="/watchHistory">История просмотра</a></li>
                <li><a href="/guestBook">Гостевая книга</a></li>
                <li><a href="/blog">Блог</a></li>
                <?php
                if($args["isAdmin"]) {
                    echo '<li><a href="/admin">Админ</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<div class="blokkok">
    <p class="lead">Тест по дисциплине: "Безопасность жизнедеятельности"</p>
    <?php
    if($args["isAuthorize"]) {
        echo '<a class="btn btn-default" href="test/table" role="button">Ответы студентов на тест</a>';
    }
    ?>
    <form id="form" class="form-horizontal" role="form" method="post">
        <div class="form-group">
            <div class="col-md-1">
                <label for="form_fio" class="control-label">Ф.И.О.:</label>
            </div>
            <div class="col-md-11">
                <input type="text" name="name" class="form-control" id="form_fio" placeholder="Фамилия Имя Отчество">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label class="control-label">Группа:</label>
            </div>
            <div class="col-md-11">
                <select name="group" id="form_group" class="form-control">
                    <option value="error" disabled selected>Выберите группу</option>
                    <optgroup label="1 Курс">
                        <option value="ИС/б-11-о">ИС/б-11о</option>
                        <option value="ИС/б-12-о">ИС/б-12о</option>
                        <option value="ИС/б-13-о">ИС/б-13о</option>
                    </optgroup>
                    <optgroup label="2 Курс">
                        <option value="ИС/б-21-о">ИС/б-21о</option>
                        <option value="ИС/б-22-о">ИС/б-22о</option>
                        <option value="ИС/б-23-о">ИС/б-23о</option>
                    </optgroup>
                    <optgroup label="3 Курс">
                        <option value="ИС/б-31-о">ИС/б-31о</option>
                        <option value="ИС/б-32-о">ИС/б-32о</option>
                        <option value="ИС/б-33-о">ИС/б-33о</option>
                        <option value="ИС/б-34-о">ИС/б-34о</option>
                        <option value="ИС/б-35-о">ИС/б-35о</option>
                    </optgroup>
                    <optgroup label="4 Курс">
                        <option value="ИС/б-41-о">ИС/б-41о</option>
                        <option value="ИС/б-42-о">ИС/б-42о</option>
                        <option value="ИС/б-43-о">ИС/б-43о</option>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
                <label>Вопрос №1 <br> Какая средняя температура человека?</label>
            </div>
            <div class="col-md-12">
                <select name="answer1" id="form_answer1" class="form-control">
                    <option value="error" disabled selected>Выберите правильный ответ</option>
                    <optgroup label="35">
                        <option value="35.4">35.4</option>
                        <option value="35.5">35.5</option>
                        <option value="35.6">35.6</option>
                    </optgroup>
                    <optgroup label="36">
                        <option value="36.6">36.6</option>
                        <option value="36.7">36.7</option>
                    </optgroup>
                    <optgroup label="37">
                        <option value="37.6">37.6</option>
                        <option value="37.7">37.7</option>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="form_answer2">Вопрос №2 <br> Сколько гектар необходимо для пространственного
                    минимума?</label>
            </div>
            <div class="col-md-12">
                <input type="text" name="answer2" class="form-control" id="form_answer2" placeholder="Ваш ответ">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4">
                <label>Вопрос №3 <br> Какого типа вентиляции не бывает?</label>
            </div>
            <div class="col-md-12">
                <label class="checkbox-inline">
                    <input type="checkbox" name="answer3" id="form_answer31" value="Автоматическая">
                    Автоматической
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="answer3" id="form_answer32" value="Искусственная">
                    Искусственной
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8">
                <input type="submit" id="submit" disabled value="Отправить" class="form-control btn btn-block btn-primary">
            </div>
            <div class="col-md-4">
                <input id="reset" type="button" value="Очистить" class="form-control btn btn-block btn-default">
            </div>
        </div>
    </form>
    <?php
    if (!empty($_POST)) {
        TestValidation::showErrors($args['errors']);
        echo '<br>';
        TestVerification::showErrors($args['responses']);
    }
    ?>
</div>
<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/time.js"></script>
<script src="/js/navigation.js"></script>
<script src="/js/functions.js"></script>
<script src="/js/modalWindow.js"></script>
<script src="/js/validationMessage.js"></script>
<script src="/js/test.js"></script>
<script src="/js/history.js"></script>
</body>
</html>