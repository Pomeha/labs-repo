<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Логин</title>
</head>
<body>
<div class="blokkok">
    <a class="btn btn-default btn-primary" href="/" role="button">Главная страница</a>
    <form method="post" class="form-horizontal">
        <div class="form-group">
            <div class="col-md-offset-4 col-md-4">
                <label for="form_login">Логин</label>
                <input type="text" class="form-control" id="form_login" name="login" placeholder="Логин">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-4">
                <label for="form_password">Пароль</label>
                <input type="password" class="form-control" id="form_password" name="password" placeholder="Пароль">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-4">
                <input id="submit" class="form-control btn btn-block btn-primary" type="submit" value="Войти">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-4">
                <a class="btn btn-default" href="/home/register" role="button" style="width: 100%">Регистрация</a>
            </div>
        </div>
    </form>
    <?php
    TestValidation::showErrors($args['errors']);
    if(!empty($args["loginError"])) {
        Home::showError($args["loginError"]);
    }
    ?>
</div>
<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/validationMessage.js"></script>
</body>
</html>