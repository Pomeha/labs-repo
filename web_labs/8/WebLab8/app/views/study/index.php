<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Учеба</title>
</head>

<body>
<nav>
    <div class='container'>
        <div class="user-banner">
            <div id="time"></div>
            <div class="user-info">
                <?php
                echo $args["userInfo"];
                ?>
            </div>
        </div>
        <div class="header">
            <ul class="nav nav-pills pull-right" id="nav">
                <li><a href="/">Главная страница</a></li>
                <li><a href="/about">Обо мне</a></li>
                <li id="interest"><a href="/interests">Мои интересы</a></li>
                <li class="active"><a href="/study">Учеба</a></li>
                <li><a href="/photoAlbum">Фотоальбом</a></li>
                <li><a href="/contact">Контакт</a></li>
                <li><a href="/test">Тест</a></li>
                <li><a href="/watchHistory">История просмотра</a></li>
                <li><a href="/guestBook">Гостевая книга</a></li>
                <li><a href="/blog">Блог</a></li>
                <?php
                if($args["isAdmin"]) {
                    echo '<li><a href="/admin">Админ</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<div class="blokkok">
    <p class="lead">Севастопольский государственный университет</p>
    <p class="lead">Институт Информационных систем и управления в технических системах</p>
    <p class="lead">Кафедра "Информационные системы"</p>

    <table id="study" class="table table-bordered table-responsive table-hover">
        <thead>
        <tr>
            <th rowspan="3">№</th>
            <th rowspan="3">Дисциплина</th>
            <th colspan="12">Часов в неделю<br>(Лекций, Лаб.раб, Практ. раб)</th>
        </tr>
        <tr>
            <th colspan="6">1 курс</th>
            <th colspan="6">2 курс</th>
        </tr>
        <tr>
            <th colspan="3">1 сем</th>
            <th colspan="3">2 сем</th>
            <th colspan="3">3 сем</th>
            <th colspan="3">4 сем</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>Экология</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>1</td>
            <td>0</td>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Высшая математика</td>
            <td>3</td>
            <td>0</td>
            <td>3</td>
            <td>3</td>
            <td>0</td>
            <td>3</td>
            <td>2</td>
            <td>0</td>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td>Русский язык и культура речи</td>
            <td>1</td>
            <td>0</td>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>4</td>
            <td>Основы дискретной математики</td>
            <td>2</td>
            <td>0</td>
            <td>1</td>
            <td>3</td>
            <td>0</td>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>5</td>
            <td>Основы программирования и алгоритмические языки</td>
            <td>3</td>
            <td>2</td>
            <td>0</td>
            <td>3</td>
            <td>3</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>6</td>
            <td>Основы экологии</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>7</td>
            <td>Теория вероятностей и математическая статистика</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>3</td>
            <td>1</td>
            <td>0</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>8</td>
            <td>Физика</td>
            <td>2</td>
            <td>2</td>
            <td>0</td>
            <td>2</td>
            <td>2</td>
            <td>0</td>
            <td>2</td>
            <td>1</td>
            <td>0</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>9</td>
            <td>Основы электротехники и электроники</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>2</td>
            <td>1</td>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>10</td>
            <td>Численные методы в информатике</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>2</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
        </tr>
        <tr>
            <td>11</td>
            <td>Методы исследования операций</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>1</td>
            <td>1</td>
            <td>0</td>
            <td>2</td>
            <td>1</td>
            <td>1</td>
        </tr>
        </tbody>
    </table>
</div>
<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/time.js"></script>
<script src="/js/navigation.js"></script>
<script src="/js/history.js"></script>
</body>
</html>
