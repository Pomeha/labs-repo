<!DOCTYPE html>
<html lang='ru'>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Администрирование</title>
</head>

<body>
<nav>
    <div class='container'>
        <div class="user-banner">
            <div id="time"></div>
            <div class="user-info">
                <?php
                echo $args["userInfo"];
                ?>
            </div>
        </div>
        <div class='header'>
            <ul class="nav nav-pills pull-right" id="nav">
                <li><a href="/">Главная страница</a></li>
                <li><a href="/about">Обо мне</a></li>
                <li id="interest"><a href="/interests">Мои интересы</a></li>
                <li><a href="/study">Учеба</a></li>
                <li><a href="/photoAlbum">Фотоальбом</a></li>
                <li><a href="/contact">Контакт</a></li>
                <li><a href="/test">Тест</a></li>
                <li><a href="/watchHistory">История просмотра</a></li>
                <li><a href="/guestBook">Гостевая книга</a></li>
                <li><a href="/blog">Блог</a></li>
                <li class="active"><a href="/admin">Админ</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class='blokkok'>
    <p class="lead">Администрирование сайта</p>
    <div class="row">
        <a class="btn btn-default" href="admin/editBlog" role="button">Редактирование блога</a>
    </div>
    <div class="row">
        <a class="btn btn-default" href="admin/questBook" role="button">Загрузка сообщений гостевой книги</a>
    </div>
    <div class="row">
        <a class="btn btn-default" href="admin/visitStatistic" role="button">Статистика посещений</a>
    </div>
</div>

<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/time.js"></script>
<script src="/js/navigation.js"></script>
<script src="/js/history.js"></script>
</body>
</html>