<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Статистика посещений</title>
</head>
<body>
<div class="blokkok">
<div class="container">
    <a class="btn btn-default btn-primary" href="/admin" role="button">Назад</a>
    <p class="lead">Статистика посещений</p>
    <?php
    echo "<div class='pull-right'>";
    for( $i = 0; $i < $args["countPages"]; $i++ ) {
        $index = $i + 1;
        if($i == $args["page"]) {
            echo "<a class='btn btn-default btn-primary' href='/admin/visitStatistic?page=$i' role='button'>$index</a>";
        } else {
            echo "<a class='btn btn-default'href='/admin/visitStatistic?page=$i' role='button'>$index</a>";
        }
    }
    echo "</div>";
    ?>
    <table class="table table-bordered table-responsive table-hover">
        <thead>
        <tr>
            <th>Дата посещения</th>
            <th>Страница</th>
            <th>Ip-address</th>
            <th>Хост</th>
            <th>Браузер</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach($args["records"] as $value) {
            echo "<tr>
                <td>$value->VisitingTime</td>
                <td>$value->VisitPage</td>
                <td>$value->IpAddress</td>
                <td>$value->HostName</td>
                <td><p>$value->BrowserName</p></td>
            </tr>";
        }
        ?>
        </tbody>
    </table>
    <?php
    echo "<div class='pull-right'>";
    for( $i = 0; $i < $args["countPages"]; $i++ ) {
        $index = $i + 1;
        if($i == $args["page"]) {
            echo "<a class='btn btn-default btn-primary' href='/admin/visitStatistic?page=$i' role='button'>$index</a>";
        } else {
            echo "<a class='btn btn-default'href='/admin/visitStatistic?page=$i' role='button'>$index</a>";
        }
    }
    echo "</div>";
    ?>
</div>
</div>
</body>
</html>