<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Загрузка записей гостевой книги из файла</title>
</head>
<body>
<div class="container">
    <a class="btn btn-default btn-primary" href="/admin" role="button">Назад</a>
    <p class="lead">Загрузка записей гостевой книги из файла</p>
    <form enctype="multipart/form-data" class="form-horizontal" method="post">
        <div class="form-group">
            <div class="col-md-12">
                <label for="form_file">Файл с записями</label>
                <input type="file" accept=".inc" name="records" id="form_file">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8">
                <input class="form-control btn btn-block btn-primary" type="submit" value="Отправить">
            </div>
            <div class="col-md-4">
                <input class="form-control btn btn-block btn-default" type="reset" value="Очистить">
            </div>
        </div>
    </form>
</div>
</body>
</html>