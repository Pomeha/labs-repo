<!DOCTYPE html>
<html lang='ru'>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Гостевая книга</title>
</head>

<body>
<nav>
    <div class='container'>
        <div class="user-banner">
            <div id="time"></div>
            <div class="user-info">
                <?php
                echo $args["userInfo"];
                ?>
            </div>
        </div>
        <div class='header'>
            <ul class="nav nav-pills pull-right" id="nav">
                <li><a href="/">Главная страница</a></li>
                <li><a href="/about">Обо мне</a></li>
                <li id="interest"><a href="/interests">Мои интересы</a></li>
                <li><a href="/study">Учеба</a></li>
                <li><a href="/photoAlbum">Фотоальбом</a></li>
                <li><a href="/contact">Контакт</a></li>
                <li><a href="/test">Тест</a></li>
                <li><a href="/watchHistory">История просмотра</a></li>
                <li class="active"><a href="/guestBook">Гостевая книга</a></li>
                <li><a href="/blog">Блог</a></li>
                <?php
                if($args["isAdmin"]) {
                    echo '<li><a href="/admin">Админ</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<div class="blokkok">
<div class="container">
    <p class="lead">Оставьте отзыв</p>
    <form id="form" class="form-horizontal" method="post">
        <div class="form-group col-md-12">
            <label for="form_fio">Фамилия Имя Отчество</label>
            <input type="text" name="name" class="form-control" id="form_fio" placeholder="Фамилия Имя Отчество">
        </div>
        <div class="form-group col-md-12">
            <label for="form_email">Email</label>
            <input type="email" name="email" class="form-control" id="form_email" placeholder="Email">
        </div>
        <div class="form-group col-md-12">
            <label for="form_review">Отзыв</label>
            <textarea class="form-control" rows="3" name="review" id="form_review" placeholder="Отзыв"></textarea>
        </div>
        <div class="form-group">
            <div class="col-md-8">
                <input type="submit" id="submit" disabled value="Отправить" class="form-control btn btn-block btn-primary">
            </div>
            <div class="col-md-4">
                <input id="reset" type="button" value="Очистить" class="form-control btn btn-block btn-default">
            </div>
        </div>
    </form>
    <p class="lead">Оставленные отзывы пользователей</p>
    <table class="table table-bordered table-responsive table-hover">
        <thead>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>Ф.И.О.</th>
            <th>Email</th>
            <th>Отзыв</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $number = 1;
        foreach($args["reviews"] as $model) {
            echo '<tr>';
            echo '<td>' . $number++ . '</td>';
            echo '<td>' . $model->Date . '</td>';
            echo '<td>' . $model->Fio . '</td>';
            echo '<td>' . $model->Email . '</td>';
            echo '<td><p style="word-wrap: normal">' . $model->Review . '</p></td>';
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
</div>
</div>
<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/time.js"></script>
<script src="/js/navigation.js"></script>
<script src="/js/history.js"></script>
<script src="/js/functions.js"></script>
<script src="/js/questBook.js"></script>
</body>
</html>