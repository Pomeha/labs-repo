<!DOCTYPE html>
<html lang='ru'>
<head>
    <meta charset='utf-8'>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/layout.css" rel="stylesheet">
    <link href="/css/iziModal.min.css" rel="stylesheet">
    <title>Сайт Акименко Владислава. Блог</title>
</head>

<body class="ff">
<nav>
    <div class='container'>
        <div class="user-banner">
            <div id="time"></div>
            <div class="user-info">
                <?php
                echo $args["userInfo"];
                ?>
            </div>
        </div>
        <div class='header'>
            <ul class="nav nav-pills pull-right" id="nav">
                <li><a href="/">Главная страница</a></li>
                <li><a href="/about">Обо мне</a></li>
                <li id="interest"><a href="/interests">Мои интересы</a></li>
                <li><a href="/study">Учеба</a></li>
                <li><a href="/photoAlbum">Фотоальбом</a></li>
                <li><a href="/contact">Контакт</a></li>
                <li><a href="/test">Тест</a></li>
                <li><a href="/watchHistory">История просмотра</a></li>
                <li><a href="/guestBook">Гостевая книга</a></li>
                <li class="active"><a href="/blog">Блог</a></li>
                <?php
                if($args["isAdmin"]) {
                    echo '<li><a href="/admin">Админ</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <p class="lead">Блог</p>
    <?php
    echo "<div class='pull-right'>";
    for( $i = 0; $i < $args["countPages"]; $i++ ) {
        $index = $i + 1;
        if($i == $args["page"]) {
            echo "<a class='btn btn-default btn-primary' href='/blog?page=$i' role='button'>$index</a>";
        } else {
            echo "<a class='btn btn-default' href='/blog?page=$i' role='button'>$index</a>";
        }
    }
    echo "</div>";
    foreach($args["records"] as $value) {
        echo "<div class='blog-record' data-blog-id='$value->Id'>";
        echo '<div class="blog-topic">';
        echo $value->Topic;
        echo '</div>';
        echo '<div class="blog-content">';
        if(!empty($value->PathToPhoto)) {
            echo '<div class="blog-photo">';
            echo "<img class= 'img-thumbnail' src='$value->PathToPhoto' alt='$value->Topic'>";
            echo '</div>';
        }
        echo '<div class="blog-message">';
        echo '<p>' . $value->Message . '</p>';
        echo '</div>';
        echo '<div class="clearfix"></div>';
        echo '<div class="blog-footer">';
        echo '<div class="blog-author">';
        echo "$value->Author $value->CurrentDate";
        echo '</div>';
        echo '<div class="blog-actions">';
        if($args['isAdmin']) {
            echo "<a class='edit-blog-action' data-blog-id='$value->Id'>Изменить</a>";
        }
        if($args["isAuthorize"]) {
            echo " <a class='add-comment-action' data-blog-id='$value->Id'>Добавить комментарий</a>";
        }
        echo '</div>';
        echo '</div>';
        echo '<div class="blog-comments">';
        if(!empty($value->Comments)) {
            foreach ($value->Comments as $comment) {
                echo '<div class="blog-comment">';
                echo '<div class="blog-comment-text">';
                echo "<p style='word-wrap: break-word'>$comment->Text</p>";
                echo '</div>';
                echo '<div class="clearfix"></div>';
                echo '<div class="blog-comment-footer">';
                echo "$comment->Fio $comment->Date";
                echo '</div>';
                echo '</div>';
            }
        }
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }
    echo "<div class='pull-right'>";
    for( $i = 0; $i < $args["countPages"]; $i++ ) {
        $index = $i + 1;
        if($i == $args["page"]) {
            echo "<a class='btn btn-default btn-primary' href='/blog?page=$i' role='button'>$index</a>";
        } else {
            echo "<a class='btn btn-default' href='/blog?page=$i' role='button'>$index</a>";
        }
    }
    echo "</div>";
    ?>
</div>

<div id="modal-add-comment" style="display: none">
    <div class="popup-container">
        <label for="comment-text">Комментарий:</label>
        <textarea type="text" class="form-control" id="comment-text" placeholder="Максимум 300 знаков" maxlength="300" rows="4"></textarea>
        <button id="send-comment-button" class="btn btn-primary">Отправить</button>
    </div>
</div>

<div id="modal-edit-blog" style="display: none">
    <div class="popup-container">
        <input type="text" class="form-control" id="blog-title" placeholder="Тема сообщения"><br>
        <label for="blog-message">Сообщение:</label>
        <textarea type="text" class="form-control" id="blog-message" placeholder="Максимум 300 знаков" maxlength="300" rows="4"></textarea>
        <button id="save-changes-button" class="btn btn-primary">Сохранить изменения</button>
    </div>
</div>

<script src="/js/jquery-3.2.0.js"></script>
<script src="/js/time.js"></script>
<script src="/js/navigation.js"></script>
<script src="/js/history.js"></script>
<script src="/js/iziModal.js"></script>
<script src="/js/blogAct.js"></script>
</body>
</html>