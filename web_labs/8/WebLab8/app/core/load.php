<?php
require APP_PATH . 'app/core/Router.php';
require APP_PATH . 'app/core/Validation.php';
require APP_PATH . 'app/core/TestValidation.php';
require APP_PATH . 'app/core/TestVerification.php';
require APP_PATH . 'app/core/BaseActiveRecord.php';
require APP_PATH . 'app/core/Database.php';
require APP_PATH . 'app/core/Controller.php';
require APP_PATH . 'app/core/View.php';
// Controllers
require APP_PATH . 'app/controllers/Home.php';
require APP_PATH . 'app/controllers/About.php';
require APP_PATH . 'app/controllers/Interests.php';
require APP_PATH . 'app/controllers/PhotoAlbum.php';
require APP_PATH . 'app/controllers/Study.php';
require APP_PATH . 'app/controllers/Contact.php';
require APP_PATH . 'app/controllers/Test.php';
require APP_PATH . 'app/controllers/WatchHistory.php';
require APP_PATH . 'app/controllers/GuestBook.php';
require APP_PATH . 'app/controllers/Blog.php';
require APP_PATH . 'app/controllers/Admin.php';
// Models
require APP_PATH . 'app/models/TestsModel.php';
require APP_PATH . 'app/models/ReviewModel.php';
require APP_PATH . 'app/models/BlogModel.php';
require APP_PATH . 'app/models/UserModel.php';
require APP_PATH . 'app/models/SiteVisitorModel.php';
require APP_PATH . 'app/models/CommentModel.php';
