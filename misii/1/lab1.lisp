;gnu clisp 2.49

(defun check(x y z)
  (if (listp x)
      (if (<= (length x) 2)
           (list x y z)
           (butlast x))
      (list y z)))
            
 
(print (check 1 2 3))
(print (check '(1 1) 2 3))
(print (check '(1) 2 3))
(print (check '(a b c) 2 3))
