(defun biniter(x)
  (let ((r nil))
       (loop
            (setq r (cons (mod x 2) r))
            (setq x (truncate x 2))
            (when (= x 0) (return r))
        )
       )
    )

(defun binrec (n &optional ac)
  (if (zerop n) 
      ac (binrec (floor n 2) (cons (mod n 2) ac))
      )
    )

(print (biniter 32))

(print (binrec 32))
